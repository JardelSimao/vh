var app = angular.module("vh", ["ui.router","datatables","datatables.bootstrap","ui.bootstrap","ngMask"]);

app.config(function($stateProvider,$sceDelegateProvider, $urlRouterProvider){


	$urlRouterProvider.otherwise('/clientes');
   	$stateProvider
		.state("clientes", {
			url: "/clientes",
			controller: "ClientesController",
			templateUrl: "js/views/clientes.html",
		})
		.state("enderecos", {
			url: "/clientes/enderecos",
			controller: "EnderecosController",
			params: {'id': null},
			templateUrl: "js/views/enderecos.html",
		});

})
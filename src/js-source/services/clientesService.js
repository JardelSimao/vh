app.factory('clientesService', function($http) {

	var _getClientes = function (){
		return $http.get("https://secure.vhsolucoes.com.br/api/clientes",{
			headers: {'x-key':'d9009d848ce1446989503784d23487d4'}
		});
	};

	var _getEstados = function (){
		return $http.get("https://secure.vhsolucoes.com.br/api/estados",{
			headers: {'x-key':'d9009d848ce1446989503784d23487d4'}
		});
	};

	var _getAdressCidades = function(id){

		return $http({
	    method: 'GET',
	    url: 'https://secure.vhsolucoes.com.br/api/estados/'+id+'/cidades', 
	    headers: {
	        'x-key': 'd9009d848ce1446989503784d23487d4'
	    	}
		});

	}

	var _salvaClientes = function (data){
		return $http({
	    method: 'POST',
	    url: 'https://secure.vhsolucoes.com.br/api/clientes', 
	    data: data,
	    headers: {
	        'x-key': 'd9009d848ce1446989503784d23487d4'
	    	}
		});
	};

	var _salvaEndereco = function (data){
		return $http({
	    method: 'POST',
	    url: 'https://secure.vhsolucoes.com.br/api/clientes/'+data.cliente_id+'/enderecos', 
	    data: data,
	    headers: {
	        'x-key': 'd9009d848ce1446989503784d23487d4'
	    	}
		});
	};

	var _editaClientes = function (data){
		return $http({
	    method: 'PUT',
	    url: 'https://secure.vhsolucoes.com.br/api/clientes/'+data.id, 
	    data: data,
	    headers: {
	        'x-key': 'd9009d848ce1446989503784d23487d4'
	    	}
		});
	};

	var _editaEndereco = function (data){
		return $http({
	    method: 'PUT',
	    url: 'https://secure.vhsolucoes.com.br/api/enderecos/'+data.id, 
	    data: data,
	    headers: {
	        'x-key': 'd9009d848ce1446989503784d23487d4'
	    	}
		});
	};

	var _getAdressClientes = function (id_cliente){
		return $http({
	    method: 'GET',
	    url: 'https://secure.vhsolucoes.com.br/api/clientes/'+id_cliente+'/enderecos', 
	    headers: {
	        'x-key': 'd9009d848ce1446989503784d23487d4'
	    	}
		});
	};

	var _excluiClientes = function (id){
		return $http.delete("https://secure.vhsolucoes.com.br/api/clientes/"+id,{
			headers: {'x-key':'d9009d848ce1446989503784d23487d4'}
		});
	};

	var _excluiCidades = function (id){
		return $http.delete("https://secure.vhsolucoes.com.br/api/enderecos/"+id,{
			headers: {'x-key':'d9009d848ce1446989503784d23487d4'}
		});
	};
	
	return {
		getClientes: _getClientes,
		salvaClientes: _salvaClientes,
		salvaEndereco: _salvaEndereco,
		excluiClientes: _excluiClientes,
		excluiCidades: _excluiCidades,
		editaClientes: _editaClientes,
		editaEndereco: _editaEndereco,
		getAdressClientes: _getAdressClientes,
		getEstados: _getEstados,
		getAdressCidades: _getAdressCidades
	};

});
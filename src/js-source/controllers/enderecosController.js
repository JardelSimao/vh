app.controller("EnderecosController", function($state, $stateParams, $uibModal, $log, $document, $scope, $compile, clientesService, DTOptionsBuilder, DTColumnBuilder, $q, $filter){

	var id_endereco;
	id_endereco =  $stateParams.id;
	id_endereco = localStorage.id_endereco;

	var language = {
	    "sEmptyTable": "Nenhum registro encontrado",
	    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
	    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
	    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
	    "sInfoPostFix": "",
	    "sInfoThousands": ".",
	    "sLengthMenu": "_MENU_ resultados por página",
	    "sLoadingRecords": "Carregando...",
	    "sProcessing": "Processando...",
	    "sZeroRecords": "Nenhum registro encontrado",
	    "sSearch": "Pesquisar ",
	    "oPaginate": {
	        "sNext": "Próximo",
	        "sPrevious": "Anterior",
	        "sFirst": "Primeiro",
	        "sLast": "Último"
	    },
	    "oAria": {
	        "sSortAscending": ": Ordenar colunas de forma ascendente",
	        "sSortDescending": ": Ordenar colunas de forma descendente"
	    }
	}

	$scope.dtOptions = DTOptionsBuilder
    .fromFnPromise(function() {
        var defer = $q.defer();

        clientesService.getAdressClientes(id_endereco).success(function(persons) {
            defer.resolve(persons);
        });
        return defer.promise;
    }).withBootstrap()
    .withLanguage(language)
    .withOption('createdRow', createdRow)
    .withOption('responsive', true);

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('logradouro').withTitle('Rua'),  
        DTColumnBuilder.newColumn('numero').withTitle('Nº'),   
        DTColumnBuilder.newColumn('bairro').withTitle('Bairro'),
        DTColumnBuilder.newColumn('cep').withTitle('Cep'),  
        DTColumnBuilder.newColumn('cidade.nome').withTitle('Cidade'),    
        DTColumnBuilder.newColumn('cidade.estado.sigla').withTitle('UF'), 
        DTColumnBuilder.newColumn(null).withTitle('Ações').notSortable()
            .renderWith(actionsHtml)

    ];

    $scope.edit = edit;
    $scope.delete = deleteRow;
    $scope.dtInstance = {};
    $scope.message = '';
    $scope.persons = {};

    function reloadData(person){
		$scope.dtInstance.reloadData();
    }


    function edit(person) {
        var enderecoData;
        enderecoData = person;

        console.log(enderecoData);

        $uibModal.open({
	      animation: $scope.animationsEnabled,
	      templateUrl: 'enderecoModalEdit.html',
	      controller: function($scope, clientesService, $uibModalInstance) {

	      		$scope.endereco = {
	      			"id":enderecoData.id,
	      			"logradouro":enderecoData.logradouro,
	      			"numero":enderecoData.numero,
	      			"bairro":enderecoData.bairro,
	      			"cep":enderecoData.cep,
	      			"estado":enderecoData.cidade.estado.id,
	      			"cidade":enderecoData.cidade.id
	      		}

	      		clientesService.getEstados().success(function(estados) {
					$scope.estados = estados;
		        });

		        clientesService.getAdressCidades(enderecoData.cidade.estado.id).success(function(cidades) {
			            $scope.cidades = cidades;
			        });

		        $scope.getCidade = function(id){

					clientesService.getAdressCidades(id).success(function(cidades) {
			            $scope.cidades = cidades;
			        });

				}

	      		$scope.cancel = function () {
					$uibModalInstance.dismiss('cancel');
				};

	            $scope.editarEndereco = function(endereco){
	            	var endereco = endereco;		
			    	var data = {
			    		"id": $scope.endereco.id,
			    		"nome": "Residencial",
			    		"complemento": "complemento",
			    		"logradouro": $scope.endereco.logradouro,
			    		"numero": $scope.endereco.numero,
			    		"bairro": $scope.endereco.bairro,
			    		"cep": $scope.endereco.cep,
			    		"cidade_id":$scope.endereco.cidade,
			    		"ativo":true
			    	}

			    	console.log(data);

			    	clientesService.editaEndereco(data).then(function successCallback(response) {
			    		reloadData();
			    		$uibModalInstance.close();
				    }, function errorCallback(response) {
				        console.log("API request failed: "+response);
				    });

			    }  
	      }
	 	})
    }

    function deleteRow(person) {
    	
        var id = person.id;
        var r = confirm("Tem certeza que deseja excluir este registro?");
		if (r == true) {
		    clientesService.excluiCidades(id).then(function successCallback(response) {
	        reloadData();
		    }, function errorCallback(response) {
		        console.log("API request failed: "+response);
		    });
		} else {

		}     
    }

    function createdRow(row, data, dataIndex) {
        $compile(angular.element(row).contents())($scope);
    }

    function actionsHtml(data, type, full, meta) {
        $scope.persons[data.id] = data;
        return '<button class="btn btn-warning" ng-click="edit(persons[' + data.id + '])">' +
            '   <i class="fa fa-edit"></i>' +
            '</button>&nbsp;' +
            '<button class="btn btn-danger" ng-click="delete(persons[' + data.id + '])" )"="">' +
            '   <i class="fa fa-trash-o"></i>' +
            '</button>';
    }

    $scope.newAdressModal = function(){
    	$uibModal.open({
	      animation: $scope.animationsEnabled,
	      templateUrl: 'enderecoModalContent.html',
	      controller: function($scope, clientesService, $uibModalInstance) {
	      		$scope.cancel = function () {
					    $uibModalInstance.dismiss('cancel');
				};
				clientesService.getEstados().success(function(estados) {
					$scope.estados = estados;
		        });

				$scope.getCidade = function(id){

					clientesService.getAdressCidades(id).success(function(cidades) {
			            $scope.cidades = cidades;
			        });

				}

	            $scope.salvarEndereco = function(endereco){
	            	var endereco = endereco;		

			    	var data = {
			    		  "cliente_id": localStorage.id_endereco,
						  "cidade_id": $scope.endereco.cidade,
						  "nome": "Residencial",
						  "logradouro": $scope.endereco.logradouro,
						  "numero": $scope.endereco.numero,
						  "complemento": "complemento",
						  "bairro": $scope.endereco.bairro,
						  "cep": $scope.endereco.cep,
						  "ativo": true
						}

			    	clientesService.salvaEndereco(data).then(function successCallback(response) {
			    		reloadData();
			    		$uibModalInstance.close();
				    }, function errorCallback(response) {
				        console.log("API request failed: "+response);
				    });

			    }  
	      }
	 	})

    }

});
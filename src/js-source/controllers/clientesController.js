app.controller("ClientesController", function($state, $stateParams, $uibModal, $log, $document, $scope, $compile, clientesService, DTOptionsBuilder, DTColumnBuilder, $q, $filter){

	var language = {
	    "sEmptyTable": "Nenhum registro encontrado",
	    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
	    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
	    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
	    "sInfoPostFix": "",
	    "sInfoThousands": ".",
	    "sLengthMenu": "_MENU_ resultados por página",
	    "sLoadingRecords": "Carregando...",
	    "sProcessing": "Processando...",
	    "sZeroRecords": "Nenhum registro encontrado",
	    "sSearch": "Pesquisar ",
	    "oPaginate": {
	        "sNext": "Próximo",
	        "sPrevious": "Anterior",
	        "sFirst": "Primeiro",
	        "sLast": "Último"
	    },
	    "oAria": {
	        "sSortAscending": ": Ordenar colunas de forma ascendente",
	        "sSortDescending": ": Ordenar colunas de forma descendente"
	    }
	}

	$scope.dtOptions = DTOptionsBuilder
    .fromFnPromise(function() {
        var defer = $q.defer();
        clientesService.getClientes().success(function(persons) {
            defer.resolve(persons);
        });
        return defer.promise;
    }).withBootstrap()
    .withLanguage(language)
    .withOption('createdRow', createdRow)
    .withOption('responsive', true);

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('nome').withTitle('Nome'),
        DTColumnBuilder.newColumn('email').withTitle('Email'),
        DTColumnBuilder.newColumn('sexo').withTitle('Sexo'),
        DTColumnBuilder.newColumn('nascimento_at').withTitle('Nasc').renderWith(function(data, type) {
			return $filter('date')(data, 'dd/MM/yyyy');
		}),
        DTColumnBuilder.newColumn(null).withTitle('Endereços').notSortable()
            .renderWith(adressHtml),
        DTColumnBuilder.newColumn(null).withTitle('Ações').notSortable()
            .renderWith(actionsHtml)

    ];

    $scope.adress = adress;
    $scope.edit = edit;
    $scope.delete = deleteRow;
    $scope.dtInstance = {};
    $scope.message = '';
    $scope.persons = {};

    function reloadData(person){
		$scope.dtInstance.reloadData();
    }

    function adress(id) {
    	var id_endereco = id;
    	$state.go("enderecos", {id: id_endereco});
    	localStorage.setItem("id_endereco", id_endereco);
    }

    function edit(person) {
        var clienteData;
        clienteData = person;

        $uibModal.open({
	      animation: $scope.animationsEnabled,
	      templateUrl: 'clientModalEdit.html',
	      controller: function($scope, clientesService, $uibModalInstance) {

	      		$scope.cliente = {
	      			"id":clienteData.id,
	      			"nome":clienteData.nome,
	      			"email":clienteData.email,
	      			"nascimento_at":clienteData.nascimento_at,
	      			"sexo":clienteData.sexo
	      		}

	      		$scope.cancel = function () {
					$uibModalInstance.dismiss('cancel');
				};

	            $scope.editarCliente = function(cliente){
	            	var cliente = cliente;		
			    	var data = {
			    		"id": $scope.cliente.id,
			    		"nome": $scope.cliente.nome,
			    		"email": $scope.cliente.email,
			    		"nascimento_at": $scope.cliente.nascimento_at,
			    		"sexo": $scope.cliente.sexo,
			    		"ativo": true
			    	}

			    	clientesService.editaClientes(data).then(function successCallback(response) {
			    		reloadData();
			    		$uibModalInstance.close();
				    }, function errorCallback(response) {
				        console.log("API request failed: "+response);
				    });

			    }  
	      }
	 	})
    }

    function deleteRow(person) {

        var id = person.id;
        var r = confirm("Tem certeza que deseja excluir este registro?");
		if (r == true) {
		    clientesService.excluiClientes(id).then(function successCallback(response) {
	        reloadData();
		    }, function errorCallback(response) {
		        console.log("API request failed: "+response);
		    });
		} else {
		    
		}     
    }

    function createdRow(row, data, dataIndex) {
        $compile(angular.element(row).contents())($scope);
    }

    function adressHtml(data, type, full, meta) {
        var id = data.id;
        return '<button class="btn btn-default btn-block" ng-click="adress(' + id + ')">' +
            '   <i class="fa fa-search"></i>' +
            '</button>&nbsp;';
    }

    function actionsHtml(data, type, full, meta) {
        $scope.persons[data.id] = data;
        return '<button class="btn btn-warning" ng-click="edit(persons[' + data.id + '])">' +
            '   <i class="fa fa-edit"></i>' +
            '</button>&nbsp;' +
            '<button class="btn btn-danger" ng-click="delete(persons[' + data.id + '])" )"="">' +
            '   <i class="fa fa-trash-o"></i>' +
            '</button>';
    }

    $scope.newClientModal = function(){
    	$uibModal.open({
	      animation: $scope.animationsEnabled,
	      templateUrl: 'clientModalContent.html',
	      controller: function($scope, clientesService, $uibModalInstance) {
	      		$scope.cancel = function () {
					    $uibModalInstance.dismiss('cancel');
				};
	            $scope.salvarCliente = function(cliente){
	            	var cliente = cliente;	

	            	var date_birth = $scope.cliente.nascimento_at;	

			    	var data = {
			    		"nome": $scope.cliente.nome,
			    		"email": $scope.cliente.email,
			    		"nascimento_at": date_birth,
			    		"sexo": $scope.cliente.sexo,
			    		"ativo": true
			    	}
			    	clientesService.salvaClientes(data).then(function successCallback(response) {
			    		reloadData();
			    		$uibModalInstance.close();
				    }, function errorCallback(response) {
				        console.log("API request failed: "+response);
				    });

			    }  
	      }
	 	})

    }



});